﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

// Функція для виведення масиву на екран
void printArray(float arr[], int n) {
    for (int i = 0; i < n; ++i)
        printf("%.1f ", arr[i]); // Форматування з одним знаком після коми
    printf("\n");
}

// Функція сортування Шелла
void shellSort(float arr[], int n) {
    int gap, i, j;
    float temp;
    // Визначаємо початковий крок
    for (gap = 1; gap < n / 3; gap = 3 * gap + 1);

    // Зменшуємо крок і сортуємо підмасиви
    for (; gap > 0; gap = (gap - 1) / 3) {
        for (i = gap; i < n; i++) {
            temp = arr[i];
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                arr[j] = arr[j - gap];
            }
            arr[j] = temp;
        }
    }
}

int main() {
    SetConsoleCP(1251); SetConsoleOutputCP(1251);
    srand(time(NULL)); // Ініціалізуємо генератор випадкових чисел

    int n;
    printf("Введіть розмірність масиву: ");
    scanf("%d", &n);

    float* arr = (float*)malloc(n * sizeof(float)); // Виділяємо пам'ять для масиву

    // Заповнюємо масив випадковими числами в діапазоні [-10;100]
    for (int i = 0; i < n; i++) {
        arr[i] = (float)(rand() % 111 - 10); // Діапазон [-10;100]
    }

    printf("Вхідний масив: ");
    printArray(arr, n);

    // Вимірюємо час сортування
    LARGE_INTEGER frequency;
    LARGE_INTEGER start, end;
    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&start);

    shellSort(arr, n);

    QueryPerformanceCounter(&end);
    double time_taken = (double)(end.QuadPart - start.QuadPart) * 1000000000.0 / frequency.QuadPart;

    printf("Відсортований масив: ");
    printArray(arr, n);

    // Виводимо час сортування з одним знаком після коми
    printf("Час сортування: %.1f наносекунд\n", time_taken);

    free(arr); // Звільняємо пам'ять, виділену для масиву
    return 0;
}
