﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

void countingSort(short arr[], int n, short minVal, short maxVal) {
    int range = maxVal - minVal + 1;
    short count[51] = { 0 }; // Масив підрахунків, розмір від -10 до 40

    for (int i = 0; i < n; i++)
        count[arr[i] - minVal]++;

    int k = 0;
    for (int i = 0; i < range; i++) {
        for (int j = 0; j < count[i]; j++) {
            arr[k++] = i + minVal;
        }
    }
}

void printArray(short arr[], int n) {
    for (int i = 0; i < n; ++i)
        printf("%d ", arr[i]);
    printf("\n");
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));

    int n;
    printf("Введіть розмірність масиву: ");
    scanf("%d", &n);

    short minVal = -10, maxVal = 40;
    short* arr = (short*)malloc(n * sizeof(short));

    for (int i = 0; i < n; i++)
        arr[i] = rand() % (maxVal - minVal + 1) + minVal;

    printf("Вхідний масив: ");
    printArray(arr, n);

    LARGE_INTEGER frequency, start, end;
    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&start);

    countingSort(arr, n, minVal, maxVal);

    QueryPerformanceCounter(&end);
    double time_taken = (double)(end.QuadPart - start.QuadPart) * 1000000000.0 / frequency.QuadPart;

    printf("Відсортований масив: ");
    printArray(arr, n);

    printf("Час сортування: %.1f наносекунд\n", time_taken);

    free(arr);
    return 0;
}
