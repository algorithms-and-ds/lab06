﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

// Функція для обміну двох елементів
void swap(float* a, float* b) {
    float temp = *a;
    *a = *b;
    *b = temp;
}

// Функція для побудови піраміди
void heapify(float arr[], int n, int i) {
    int largest = i; // Ініціалізуємо найбільший елемент як корінь
    int left = 2 * i + 1; // Лівий нащадок
    int right = 2 * i + 2; // Правий нащадок

    // Якщо лівий нащадок більший за корінь
    if (left < n && arr[left] > arr[largest])
        largest = left;

    // Якщо правий нащадок більший за найбільший елемент на даний момент
    if (right < n && arr[right] > arr[largest])
        largest = right;

    // Якщо найбільший елемент не є коренем
    if (largest != i) {
        swap(&arr[i], &arr[largest]);

        // Рекурсивно викликаємо функцію heapify для піддерева
        heapify(arr, n, largest);
    }
}

// Основна функція для сортування масиву
void heapSort(float arr[], int n) {
    // Побудова піраміди (перегрупування масиву)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Один за одним видаляємо елементи з піраміди
    for (int i = n - 1; i >= 0; i--) {
        // Переміщаємо поточний корінь в кінець
        swap(&arr[0], &arr[i]);

        // Викликаємо функцію heapify на зменшеному масиві
        heapify(arr, i, 0);
    }
}

// Функція для виведення масиву на екран
void printArray(float arr[], int n) {
    for (int i = 0; i < n; ++i)
        printf("%.1f ", arr[i]); // Форматування з одним знаком після коми
    printf("\n");
}

int main() {
    SetConsoleCP(1251); SetConsoleOutputCP(1251);
    srand(time(NULL)); // Ініціалізуємо генератор випадкових чисел

    int n;
    printf("Введіть розмірність масиву: ");
    scanf("%d", &n);

    float* arr = (float*)malloc(n * sizeof(float)); // Виділяємо пам'ять для масиву1

    // Заповнюємо масив випадковими числами в діапазоні [-200;10]
    for (int i = 0; i < n; i++) {
        arr[i] = (float)(rand() % 211 - 200) / 10.0;
    }

    printf("Вхідний масив: ");
    printArray(arr, n);

    // Вимірюємо час сортування
    LARGE_INTEGER frequency;
    LARGE_INTEGER start, end;
    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&start);

    heapSort(arr, n);

    QueryPerformanceCounter(&end);
    double time_taken = (double)(end.QuadPart - start.QuadPart) * 1000000000.0 / frequency.QuadPart;

    printf("Відсортований масив: ");
    printArray(arr, n);

    // Зменшуємо кількість знаків після коми до 1
    printf("Час сортування: %.1f наносекунд\n", time_taken);

    free(arr); // Звільняємо пам'ять, виділену для масиву
    return 0;
}
